"""
Created on Wed Jul 05 11:37:07 2023
@author: kgerarduzzi
Code pour créer un fichier st de canaux.
Géometrie simplifier et uniforme sur le long du canal 
Possibilité de mettre une pente
input :
    -liste de point rive droite txt
    -liste de point rive gauche txt
    -liste des pks txt
    -liste des differents z de chaque cross section     ex: Z=[1.72, 1.545, 0.745, -2.5, -2.5, 1.17]
    -liste des pourcentages de distance entre chaque points (pour definir l'abscisse)
    pourcentage entre point 1 point 2 =(distance p1 p2*100)/distance rive gauche rive droite
    ex:pourcentage=[0.08, 0.11, 0.16, 0.32]
    -
"""
import matplotlib.pyplot as plt
import math
class Point():
    def __init__(self, values=[0.0,0.0,0.0], tag =''): #x,y,z
        self.values = values
        self.tag = tag
user_input = input("Voulez-vous que le modele soit fermer (oui/non) ? ").lower()
if user_input in ["y", "yes", "oui"]:
    path='/home/kgerarduzzi/Bureau/Canaux donnée/donnée point canaux relié/'
else :
    path='/home/kgerarduzzi/Bureau/Canaux donnée/List point pour fichier st/bon/txt/'
# def fonction_sign(number):
#     if number < 0:
#         return -1
#Calcul des proportions :
##for i in range (0,5):
##    a = float(input("Entrez la valeur de a : "))
##    resultat = a * 100 / 6.15 #6.8 #6.85
##    print(resultat)
##liste_vt=[-2.5,1.72,93,62,75,79]#[h, h1, p2, p3, p6, p7]

### Création de la liste des hauteur pour une cross section type des canaux NL KT VT :
##liste_VT=[-2.5,1.72,7,39,26,22]
##liste_KT=[-1.65,1.95,5,54,39,14]
##liste_NL=[-0.061,1.59,29,84,37,21] 
##def calculer_vecteur_hauteur(liste):
##    h, h1, p2, p3, p6, p7 = liste
##    print('h = ',h,' h1 = ', h1,' p2 = ', p2,' p3 = ', p3,' p6 = ', p6,' p7 = ', p7 )
##    a2 = p2 * h / 100
##    a3 = p3 * h / 100
##    a6 = p6 * h / 100
##    a7 = p7 * h / 100
##    Z = [h1, h1 + a2,h1 + a3, h, h, h1 + a6, h1 + a7]
####    Znl = [h1,h1, h1 + a2,h1 + a3, h, h, h1 + a6, h1 + a7]
##    return Zn
##Z = calculer_vecteur_hauteur(liste_NL)

def create_list_from_file(file_path):
    pk_VT = []    
    with open(file_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            value = float(line.strip().rstrip(','))
            if not pk_VT:
                pk_VT.append(value)
            else:
                pk_VT.append(value - pk_VT[0])

    return pk_VT

def nom_canal(path,user_input): #selectionne les bonnes données
    nom=input('nom canal : VT, KT, NL ou coupure: ')
    Z,pk= [],[]
    pourcentage=[]
    fichier_rive_gauche = ''
    fichier_rive_droite = ''
    pk_NL=[0.0000, 164.0000,461.0000,724.0000,1014.0000,1759.0000,2158.0000,2415.0000,2584.0000,2758.0000,2994.0000,3211.0000]
    pk_Coupure=[0.0000, 386.0000,1104.0000,1427.0000]
    if user_input in ["y", "yes", "oui"]:
        pk_KT=create_list_from_file('/home/kgerarduzzi/Bureau/Canaux donnée/donnée point canaux relié/pk_KT.txt')
        pk_VT=create_list_from_file('/home/kgerarduzzi/Bureau/Canaux donnée/donnée point canaux relié/pk_VT.txt')
        fer_ouv='2'
    else :
        pk_VT=[0.0000, 425.0000,877.0000,988.0000,1269.0000,1513.0000,1704.0000,1979.0000,2289.0000,2740.0000,3257.0000,3708.0000,4066.0000,4191.0000,4459.0000,]
        pk_KT=[0.0000, 346.0000,690.0000,1052.0000,1929.0000,2485.0000,3346.0000,3654.0000,3776.0000]
        fer_ouv=''

    if nom=='VT' or nom=='Vt' or nom=='vt':
        print('VT canal : \n')
        fichier_rive_gauche = path+"List_point_VT_Rd_"+fer_ouv+".txt" #"List_point_VT_Rd_2.txt" pour fermer 
        fichier_rive_droite = path+"List_point_VT_Rg_"+fer_ouv+".txt"
        output_st_file=path+"VT"+fer_ouv+"_st.st"
        Z=[1.72, 1.545, 0.745, -2.5, -2.5, 1.0699999999999998, 1.17]
        pourcentage=[0.08, 0.11, 0.16, 0.32, 0.11]
        pk=pk_VT
        
    elif nom=='KT' or nom=='Kt' or nom=='kt':
        print('KT canal : \n')
        fichier_rive_gauche = path+"List_point_KT_Rd_"+fer_ouv+".txt"
        fichier_rive_droite = path+"List_point_KT_Rg_"+fer_ouv+".txt"
        output_st_file=path+"KT"+fer_ouv+"_st.st"
        Z=[1.95, 1.8675, 1.0590000000000002, -1.65, -1.65, 1.3065, 1.7189999999999999]
        pourcentage=[0.07, 0.24, 0.06, 0.24, 0.28]        
        pkav=33427
        if user_input in ["y", "yes", "oui"]:
            pk=[value + pkav for value in pk_KT]
        else :
            pk=pk_KT
            
    elif nom=='NL' or nom=='NL' or nom=='nl':
        print('NL canal : \n')
        fichier_rive_gauche = path+"List_point_NL_Rd.txt"
        fichier_rive_droite = path+"List_point_NL_Rg.txt"
        output_st_file=path+"NL_st_fentes.st"
        Z=[1.59, 1.59, 1.57231, 1.5387600000000001, -0.061,-0.061,-25.0,-26.0,-0.061, -0.061, 1.56743, 1.57719]
        pourcentage=[0.16, 0.15, 0.14, 0.19, 0.10,0.0,0.001,0.0,0.099,0.07] # [0.16, 0.15, 0.14, 0.19, 0.20,0.07]
        pk=pk_NL
    elif nom=='COUPURE' or nom=='Coupure' or nom=='coupure':
            print('Coupure canal : \n')
            fichier_rive_gauche = path+"List_point_Coupure_Rd.txt"
            fichier_rive_droite = path+"List_point_Coupure_Rg.txt"
            output_st_file=path+"Coupure_st.st"
            Z=[1.67, -3.33, -3.33, 1.67]
            pourcentage=[0, 1] 
            pk=pk_Coupure
    return fichier_rive_gauche, fichier_rive_droite,pourcentage,Z,pk,output_st_file

def calculer_points(fichier_rive_gauche, fichier_rive_droite,pourcentages):#calcul x et y
    liste_points = []
    L_Rd = []
    L_Rg = []
    with open(fichier_rive_gauche, 'r') as f_rive_gauche, open(fichier_rive_droite, 'r') as f_rive_droite:
        lignes_rive_gauche = f_rive_gauche.readlines()
        lignes_rive_droite = f_rive_droite.readlines()

        for i in range(min(len(lignes_rive_gauche), len(lignes_rive_droite))):
            ligne_rive_gauche = lignes_rive_gauche[i].strip().split(',')
            ligne_rive_droite = lignes_rive_droite[i].strip().split(',')

            x_rive_gauche = float(ligne_rive_gauche[0].strip('"'))
            y_rive_gauche = float(ligne_rive_gauche[1].strip('"'))
            L_Rg.append([x_rive_gauche, y_rive_gauche])

            x_rive_droite = float(ligne_rive_droite[0].strip('"'))
            y_rive_droite = float(ligne_rive_droite[1].strip('"'))
            L_Rd.append([x_rive_droite, y_rive_droite])

            distance_points = ((x_rive_droite - x_rive_gauche) ** 2 + (y_rive_droite - y_rive_gauche) ** 2) ** 0.5
            
            # Calcul des pourcentages
            total_pourcentages = sum(pourcentages)
            distance_totale = distance_points / total_pourcentages

            # Calcul des distances di
            distances_di = [pourcentage * distance_totale for pourcentage in pourcentages]

            point_1 = [x_rive_gauche, y_rive_gauche]
            point_2 = [x_rive_droite, y_rive_droite]

            # Calcul des coordonnées des points intermédiaires sur la droite
            points_ligne = [point_1]
            for i in range(len(distances_di)):
                x_point = points_ligne[-1][0] + distances_di[i] * (x_rive_droite - x_rive_gauche) / distance_points
                y_point = ((y_rive_droite - y_rive_gauche) / (x_rive_droite - x_rive_gauche)) * (
                            x_point - x_rive_gauche) + y_rive_gauche
                points_ligne.append([x_point, y_point])
            points_ligne.append(point_2)

            liste_points.append(points_ligne)
            # Vérification des distances di
            # somme_di = sum(distances_di)
            # if round(somme_di, 2) == round(distance_totale, 2):
            #     print("OK \nDistance rive gauche - rive droite:", round(distance_totale, 2))
                # print("Somme des di:", round(somme_di, 2))
    return liste_points, L_Rd, L_Rg,len(lignes_rive_gauche)
 
def tracer_points(liste_points, L_Rd, L_Rg):
    for points_ligne in liste_points:
        x_coords = [point[0] for point in points_ligne]
        y_coords = [point[1] for point in points_ligne]
        plt.scatter(x_coords[:-2], y_coords[:-2], marker='*',s=150)
##        plt.scatter(x_coords[-1], y_coords[-1], marker='*',color='black',s=150)

        # Tracer la droite entre les points initiaux de la rive droite et de la rive gauche
        plt.plot(x_coords, y_coords, linestyle='dashed', color='black')

    x_rive_gauche = [point[0] for point in L_Rg]
    y_rive_gauche = [point[1] for point in L_Rg]
    x_rive_droite = [point[0] for point in L_Rd]
    y_rive_droite = [point[1] for point in L_Rd]

    plt.scatter(x_rive_gauche, y_rive_gauche, marker='.', color='blue', s=100)
    plt.scatter(x_rive_droite, y_rive_droite, marker='.', color='red', s=100)

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Tracé des points et droites')
    plt.grid(True)
    ax = plt.gca()
    ax.set_aspect('equal', adjustable='box')
    plt.show()

#Calcul les Z avec la pente et crée la cross section avec XYZ
def add_z_coordinates(liste_point, z_values): 
    #list pt [[Cs1],[Cs2],[[x1cs3,y1cs3],[x2cs3,y2cs3]]
    new_liste_point,new_cross_section = [],[]
    slope=0.001 #0.1%
    z_before=z_values
    for j, point in enumerate (liste_point[0]):#Pour la 1ere cross section
        new_point = point + [z_values[j]]
        new_cross_section.append(new_point)
    new_liste_point.append(new_cross_section)
    # print(new_cross_section)
    for i, cross_section in enumerate(liste_point[1:], start=1):#Parcours les cross section sauf la premiere
        new_cross_section = []
        
        for j, point in enumerate (cross_section):#Parcours les points dans la cross section
            sign = sign = math.copysign(1, z_before[j])
            if sign==-1:            
                new_z = z_before[j] * (1 + slope)
                z_before[j]=new_z
            else :
                new_z = z_before[j] * (1 - slope)#calcul de Z
                z_before[j]=new_z
            new_point = point + [new_z]
            new_cross_section.append(new_point)#on cre la liste avec x,y,z
        print(new_cross_section,'\n')
        new_liste_point.append(new_cross_section)
                        
    return new_liste_point

# Création de l'entête  tel que :
#exemple_entete= ['     1     0     0    37       0.0000   Cs1\n', '     2     0     0    37    4000.0000   Cs2\n']
def generer_entete(num_cs, nombre_pt, liste_pk):
    liste_entete = []
    for i in range(num_cs):
        num_incre = i + 1
        pk_i = liste_pk[i]
        cs_i = f'Cs{num_incre}'
        element_cs = f'     {num_incre:5}     0     0    {nombre_pt:2}    {pk_i:.4f}   {cs_i}\n'
        liste_entete.append(element_cs)
    return liste_entete

def write_results(bief_xyz, entetes, New_st_file):
    with open(New_st_file, 'w') as f:
        for s,section in enumerate(bief_xyz): #indice,object a l'interieur
            if s != 0: f.write('     999.9990     999.9990     999.9990 \n')
            f.write(entetes[s])
            for p,point in enumerate(section):
                f.write(f" {point.values[0]:12.4f} {point.values[1]:12.4f} {point.values[2]:12.4f} {point.tag} \n")
        f.write('     999.9990     999.9990     999.9990 \n')
    print(f"Les résultats ont été enregistrées dans le fichier : {New_st_file}")

# Utilisations fonction
fichier_rive_gauche, fichier_rive_droite,pourcentage,Z,pk,output_st_file=nom_canal(path,user_input)
liste_points,L_Rd,L_Rg,nb_cs = calculer_points(fichier_rive_gauche, fichier_rive_droite,pourcentage)
tracer_points(liste_points,L_Rd, L_Rg) #plot
#liste point :[[[685950.436, 1198633.433], [685987.4513, 1198618.5069],[686056.194, 1198590.787]], [[x,y],[x,y]]]
print('len pk',len(pk))
nombre_pt = len (liste_points[0])


entetes = generer_entete(nb_cs, nombre_pt, pk)
# print(resultat)
# for element in resultat:
#     print(element)
list_xyz=add_z_coordinates(liste_points,Z)

bief_xyz = [[] for _ in range(nb_cs)]  # Initialize an empty list for each cross-section
tag=''
for cs in range (len(list_xyz)): # Changement du tag pour rg et rd
    first=1
    # print('\n',len(list_xyz))
    for p in range (len(list_xyz[0])):
        if p==0: #first
            tag='rg'
            first=0 
        elif p==5 : #nl
            tag='fg'
        elif p==6 : #nl
            tag='fd' 
        elif p==(len(list_xyz[0])-1): #last
            tag='rd'
        else:
            tag=''
        bief_xyz[cs].append(Point(list_xyz[cs][p], tag))#utilise la classe point 

write_results(bief_xyz,entetes,output_st_file)